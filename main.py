from fastapi import FastAPI
from config.database import engine, Base
from middlewares.error_handlers import ErrorHandler
from routers import movies, users

app = FastAPI()
app.title = "Mi aplicación con FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movies.router)
app.include_router(users.router)

Base.metadata.create_all(bind=engine)




